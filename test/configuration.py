WEBSITE_URL = "http://localhost"

BASE_URL = "https://portal-sensorfeed.mediumone.com"


API_URL = "https://api-dev.mediumone.com/v2/"


# Web driver wait times.
SHORT_WAIT = 1
MID_WAIT = 5
LONG_WAIT = 30

# Credentials for Super Admin Login.
SUPER_ADMIN_EMAIL = "dev-sensorfeed@medium.one"
SUPER_ADMIN_NAME = "Dev"
SUPER_ADMIN_PASSWORD = "kLCpdEkBG1XhQNLO"

# Credentials for Tenant as Admin Login.
# This user is considered as already existing user(already added to the Selenium Test Account in the portal).
# TENANT_ADMIN_EMAIL = "somya.bansal+test1@medium.one"
TENANT_ADMIN_EMAIL = "somya.bansal+test6@medium.one"
TENANT_ADMIN_NAME = "Selenium_test_tenant_admin"
# TENANT_ADMIN_PASSWORD = "z68xhNvO"
TENANT_ADMIN_PASSWORD = "IvJPtyrx"

# Credentials for Tenant as User Login.
# This user is considered as already existing user(already added to the Selenium Test Account in the portal).
TENANT_USER_EMAIL = "somya.bansal+test2@medium.one"
TENANT_USER_NAME = "Selenium_test_tenant_user"
TENANT_USER_PASSWORD = "PLMhuk9h"

# Manage Accounts.
account_name = "test_account"
device = "0000001"
new_account_name = "new_test_account"

# Inviting(Adding)/Deleting the below user under Manage Account Users tab as User(Tenant User).
user_id = "selenium+test@medium.one"
user_name = "Selenium Test"

# Linking/Un-linking the below user in/from Manage Accounts(SA) as User(Tenant User).
# This user is considered as already invited(existing) user but not linked to the newly created account on portal.
user_id_link = "selenium+test1@medium.one"
user_name_link = "Selenium Test1"

# To test a non existing user.
new_user_id = "selenium+test4@medium.one"
new_user_name = "Selenium Test4"

# Configure Stripe Billing Credentials(Somya's stripe account credentials).
secret_key = "sk_test_kTWoTyIhEh4PkaShLQHEjsmQ"
publishable_key = "pk_test_awENL9xrEWfxUBT35pnlcuMi"
invalid_secret_key = "inValid_sk_test_kTWoTyIhEh4PkaShLQHEjsmQ"
invalid_publishable_key = "inValid_pk_test_awENL9xrEWfxUBT35pnlcuMi"

# Link IoT Platform Credentials.
api_base_url = "https://api.mediumone.com"
invalid_api_base_url = "https://api.mediumone"
api_business_username = "test_sensorfeed"
api_business_password = "Test_sensorfeed"
api_key = "BTWC43DYT5YBFAI7GS3TA6BQGZQTSNZUGVSWMNBYGQ3DAMBQ"

# Manage Labels.
tag_select = "sensor_data.Moti"
label_name = "Test_Motion"

# Configuration Portal Details.
portal_name = " "
vendor_url = " "
logo_url = "https://static.wixstatic.com/media/f58ecc_5155483253ad427292666f8b50986961~mv2.png"
background_url = "https://static.wixstatic.com/media/f58ecc_7a18cd29ba61483996f77fff774b4b34~mv2.png"
favicon_url = "https://static.wixstatic.com/media/f58ecc_b74ec3ab1af441f6a7e31ef02d8eae5b~mv2.png"

# Tenant as Admin(TA) and Tenant as User(TU) Requirements:

# Device Summary(both under TA and TU).
new_place = "Test_Device"
old_place = "Home"

# Tenant as Admin Requirements only:

# Manage/Register Device.
new_device = "new_test_device"
device_err_msg_1 = "Device is already in use. Please contact your administrator to reassign the device."
device_err_msg_2 = "Device was not found in IoT Platform, has the device been activated?"

# Adding an new user(inviting a new user and adding it to a particular account, here we are adding it in account
# "Selenium Test") in Manage Users(under TA) as TU only.
ta_user_id_add = "selenium+test2@medium.one"
ta_user_name_add = "Selenium Test2"

# Trying to add an user in Manage Users(under TA) as TU only which is already linked to that operating account
# (considering Selenium Test account here).
# This user is considered as already invited(existing) user.
ta_user_id = "selenium+test3@medium.one"
ta_user_name = "Selenium Test3"

package1 = "null (0 USD per month)"
package2 = "null (100 USD per month)"
credit_card_detail = "0000 1111 2222 3333"
cvc = "123"
expire_month = "01"
expire_year = "2020"
current_password = "IvJPtyrx"

# Manage Dashboards
page_name = "Test Page"


