from pages.login import Login
from pages.homepage import HomePageSuperAdmin, HomePageTenantAdmin, HomePageTenantUser
from test.configuration import *
import logging

import time
import unittest
from selenium import webdriver

log = logging.getLogger()


class BaseTest(unittest.TestCase):
    def setUp(self):
        # log.debug("setting up")
        self.driver = webdriver.Chrome()
        self.driver.set_window_position(0, 0)
        self.driver.set_window_size(1280, 768)
        # self.driver = webdriver.Firefox()
        # self.driver.implicitly_wait(MID_WAIT)

    def page_should_be(self, cls):
        self.page = cls(self)
        self.assertTrue(self.page.verify_page())
        # return cls(self)

    def tearDown(self):
        log.debug("tearing down")
        self.driver.close()


class SuperAdminLoggedIn(BaseTest):
    def setUp(self):
        super(SuperAdminLoggedIn, self).setUp()

        log.debug("login in")
        self.page_should_be(Login)

        self.page.login(SUPER_ADMIN_EMAIL, SUPER_ADMIN_PASSWORD)
        self.page_should_be(HomePageSuperAdmin)
        # time.sleep(1)

        '''
        if self.page.TopMenu.get_current_tenant_name() != TENANT_NAME1:
            self.page.TopMenu.switch_tenant(TENANT_NAME1)
            self.page_should_be(pages.Dashboard)
        self.assertEqual(self.page.TopMenu.get_current_tenant_name(), TENANT_NAME1)
        '''

    def tearDown(self):
        # log.debug("login out")
        time.sleep(1)
        # self.page_should_be(Pages.Dashboard)
        # log out from whatever page we are on
        self.page.TopMenu.logout()
        time.sleep(1)
        self.assertEqual(self.driver.current_url, BASE_URL + "/login")
        super(SuperAdminLoggedIn, self).tearDown()


class TenantAdminLoggedIn(BaseTest):
    def setUp(self):
        super(TenantAdminLoggedIn, self).setUp()

        log.debug("login in")
        self.page_should_be(Login)

        self.page.login(TENANT_ADMIN_EMAIL, TENANT_ADMIN_PASSWORD)
        time.sleep(3)
        self.page_should_be(HomePageTenantAdmin)
        time.sleep(3)

    def tearDown(self):
        # log.debug("login out")
        time.sleep(1)
        # self.page_should_be(Pages.Dashboard)
        # log out from whatever page we are on
        self.page.TopMenu.logout()
        time.sleep(1)
        self.assertEqual(self.driver.current_url, BASE_URL + "/login")
        super(TenantAdminLoggedIn, self).tearDown()


class TenantUserLoggedIn(BaseTest):
    def setUp(self):
        super(TenantUserLoggedIn, self).setUp()

        log.debug("login in")
        self.page_should_be(Login)

        self.page.login(TENANT_USER_EMAIL, TENANT_USER_PASSWORD)
        time.sleep(3)
        self.page_should_be(HomePageTenantUser)
        time.sleep(3)

    def tearDown(self):
        # log.debug("login out")
        time.sleep(1)
        # self.page_should_be(Pages.Dashboard)
        # log out from whatever page we are on
        self.page.TopMenu.logout()
        time.sleep(1)
        self.assertEqual(self.driver.current_url, BASE_URL + "/login")
        super(TenantUserLoggedIn, self).tearDown()


class OnBoarding(unittest.TestCase):
    def setUp(self):
        # log.debug("setting up")
        self.driver = webdriver.Chrome()
        self.driver.set_window_position(0, 0)
        self.driver.set_window_size(1280, 768)

    def page_should_be(self, cls, partner, board_type, kit):
        self.page = cls(self, partner, board_type, kit)
        self.assertTrue(self.page.verify_page())

    def tearDown(self):
        # log.debug("tearing down")
        self.driver.close()
