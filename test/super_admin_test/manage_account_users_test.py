from test.base_test import SuperAdminLoggedIn
from pages import homepage
from pages.page_objects.super_admin import manage_account_users
from test.configuration import user_id, user_name, ta_user_id, ta_user_name

import time

# TODO: To test whether the deleted account exist or not


class ManageAccountUsers(SuperAdminLoggedIn):

    def setUp(self):
        super(ManageAccountUsers, self).setUp()
        self.page_should_be(homepage.HomePageSuperAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.manage_account_users)
        self.page_should_be(manage_account_users.UserList)

# TODO: This method should have expected fail. As at the beginning, there will not be any account in the portal.
    # Test method to display the list of existing users in the portal.
    def test_a_users_list(self):
        self.page_should_be(manage_account_users.UserList)
        time.sleep(2)
        self.driver.refresh()
        time.sleep(1)
        self.page_should_be(manage_account_users.UserList)
        # Get list of all users in the portal.
        users = [self.page.UsersList.get_cell_from_row(r, 1).text for r in self.page.UsersList.get_rows()]
        self.assertGreater(len(users), 0, "No user connected to the portal")

    # Test method to invite a new user and delete it to/from the portal.
    def test_invite_and_delete_user(self):
        self.page_should_be(manage_account_users.UserList)

        try:
            # Invite a new user on the portal.
            self.page.Buttons.invite.click()
            self.page_should_be(manage_account_users.InviteUser)
            time.sleep(2)
            # Click the invite button without entering the required credentials, to test the error message.
            self.page.Buttons.invite_invite.click()
            time.sleep(2)
            # Invite a new user on the portal by entering the required credentials.
            self.page.invite_new_user(user_id, user_name)
            time.sleep(3)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_account_users.UserList)
            # Search for invited user.
            self.page.search_user(user_id)
            # Get list of all users in the portal.
            users = [self.page.UsersList.get_cell_from_row(r, 1).text for r in self.page.UsersList.get_rows()]
            # Verify the presence of invited user in the portal.
            self.assertEqual(len(users), 1, "the number of users do not match")
            self.assertIn(user_name, users, "the user list doesn't contains that user")

        finally:
            self.driver.get(manage_account_users.MANAGE_ACCOUNT_USERS_LIST)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(1)
            self.page_should_be(manage_account_users.UserList)
            # Search for the user.
            self.page.search_user(user_id)
            # Get list of all users in the portal.
            user_ids = [self.page.UsersList.get_cell_from_row(r, 2).text for r in self.page.UsersList.get_rows()]
            # Check if the user exists in users list , if yes, then process to delete that user.
            if user_id in user_ids:
                # Select the searched user.
                self.page.select_user(user_id)
                time.sleep(2)
                self.user_id = user_id
                self.page_should_be(manage_account_users.DeleteUser)
                time.sleep(2)
                self.driver.refresh()
                time.sleep(1)
                self.page_should_be(manage_account_users.DeleteUser)
                # Delete the selected user.
                self.page.delete_user()
                self.page_should_be(manage_account_users.UserList)
                # Click the final ok.
                self.page.remove_user_ok()

    # Test method for trying to invite an existing user, cancel the process and search for an non existing user.
    def test_invite_cancel_search_user(self):
        self.page_should_be(manage_account_users.UserList)
        # Invite the user.
        self.page.Buttons.invite.click()
        time.sleep(1)
        self.page_should_be(manage_account_users.InviteUser)
        # Try inviting an existing user, to test the error message.
        self.page.invite_new_user(ta_user_id, ta_user_name)
        time.sleep(2)
        # Cancel inviting the user.
        self.page.Buttons.cancel_invite.click()
        time.sleep(3)
        self.page_should_be(manage_account_users.UserList)
        time.sleep(1)
        self.driver.refresh()
        time.sleep(2)
        # Search for a non existing user.
        self.page.search_user(user_id)
        # Get the list of all users in the portal.
        users = [self.page.UsersList.get_cell_from_row(r, 1).text for r in self.page.UsersList.get_rows()]
        # Verify for no such user exists.
        self.assertEqual(len(users), 1, "the number of user do not match")
        self.assertNotIn(user_name, users, "the users list contains the user")
        self.assertIn("No matching records found", users, "the user list contains that user")
