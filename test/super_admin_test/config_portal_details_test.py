from test.base_test import SuperAdminLoggedIn, HomePageSuperAdmin, Login
from pages import homepage
from pages.page_objects.super_admin import config_portal_details
from test.configuration import portal_name, vendor_url, logo_url, background_url, favicon_url, SUPER_ADMIN_EMAIL, \
    SUPER_ADMIN_PASSWORD, BASE_URL


import time


class ConfigPortal(SuperAdminLoggedIn):

    def setUp(self):
        super(ConfigPortal, self).setUp()
        self.page_should_be(homepage.HomePageSuperAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.config_portal_details)
        self.page_should_be(config_portal_details.ConfigPortalDetails)

    # Test to load the original(or remove the existing) logo and background(Medium One) configurations.
    def test_load_original_config(self):
        self.page_should_be(config_portal_details.ConfigPortalDetails)
        # Load original configurations of logo and background URL.
        logo, bg = self.page.load_original()

        # Logout to verify the changes.
        self.page.TopMenu.logout()
        time.sleep(1)
        # Verify URL of the login page.
        self.assertEqual(self.driver.current_url, BASE_URL + "/login")

        try:
            # Extract the background.
            elem = self.driver.find_element_by_xpath("/html/body")
            bg_url = elem.get_attribute("background")
            # Verify the changes in background URL.
            self.assertEqual(bg, bg_url, "Background URL doesn't match")

            # Extract the logo.
            elem = self.driver.find_element_by_xpath('//*[@id="box-login"]/img')
            lg_url = elem.get_attribute("src")
            # Verify the changes in logo URL.
            self.assertEqual(logo, lg_url, "Logo URL doesn't match")
        finally:
            self.page_should_be(Login)
            # Login as a Super Admin.
            self.page.login(SUPER_ADMIN_EMAIL, SUPER_ADMIN_PASSWORD)
            time.sleep(2)
            self.page_should_be(HomePageSuperAdmin)

    # Test to save/re-save new logo and background configurations.
    def test_save_config_detail(self):
        self.page_should_be(config_portal_details.ConfigPortalDetails)
        # Configure URLs for logo, background and favicon.
        self.page.configure(portal_name, vendor_url, logo_url, background_url, favicon_url)

        # Logout to verify the changes.
        self.page.TopMenu.logout()
        time.sleep(1)
        # Verify URL of the login page.
        self.assertEqual(self.driver.current_url, BASE_URL + "/login")

        try:
            # Extract the background.
            elem = self.driver.find_element_by_xpath("/html/body")
            bg_url = elem.get_attribute("background")
            # Verify the changes in background URL.
            self.assertEqual(background_url, bg_url, "Background URL doesn't match")

            # Extract the logo.
            elem = self.driver.find_element_by_xpath('//*[@id="box-login"]/img')
            lg_url = elem.get_attribute("src")
            # Verify the changes in logo URL.
            self.assertEqual(logo_url, lg_url, "Logo URL doesn't match")
        finally:
            self.page_should_be(Login)
            # Login as a Super Admin.
            self.page.login(SUPER_ADMIN_EMAIL, SUPER_ADMIN_PASSWORD)
            time.sleep(2)
            self.page_should_be(HomePageSuperAdmin)
