from test.base_test import SuperAdminLoggedIn
from pages import homepage
from pages.page_objects.super_admin import manage_labels
from test.configuration import tag_select, label_name

import time

# TODO: To test whether the deleted label exist or not in search box


# Test to create a new label and delete it.
class ManageLabels(SuperAdminLoggedIn):

    def setUp(self):
        super(ManageLabels, self).setUp()
        self.page_should_be(homepage.HomePageSuperAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.manage_labels)
        self.page_should_be(manage_labels.Labels)

# TODO: This method should have expected fail. As at the beginning, there will not be any account in the portal.
    # Test method to display the list of existing accounts in the portal.
    def test_a_label_list(self):
        self.page_should_be(manage_labels.Labels)
        time.sleep(2)
        self.driver.refresh()
        time.sleep(1)
        self.page_should_be(manage_labels.Labels)
        # Get list of all accounts in the portal.
        labels = [self.page.LabelsList.get_cell_from_row(r, 1).text for r in self.page.LabelsList.get_rows()]
        self.assertGreater(len(labels), 0, "No label created in the portal")

    # Test method to create a new label, select a label and  delete the selected label.
    def test_create_and_delete_label(self):
        self.page_should_be(manage_labels.Labels)
        self.driver.refresh()
        time.sleep(2)
        # Create a new label.
        self.page.Buttons.create_label.click()
        self.page_should_be(manage_labels.CreateLabel)

        try:
            time.sleep(1)
            self.page.create_label(tag_select, label_name)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(1)
            self.page_should_be(manage_labels.Labels)
            # Search for invited user.
            self.page.search_label(label_name)
            # Get list of all users in the portal.
            labels = [self.page.LabelsList.get_cell_from_row(r, 1).text for r in self.page.LabelsList.get_rows()]
            # Verify the presence of invited user in the portal.
            self.assertEqual(len(labels), 1, "the number of labels do not match")
            self.assertIn(label_name, labels, "the label list doesn't contains the label")

        finally:
            self.driver.get(manage_labels.MANAGE_LABELS_LIST)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(1)
            self.page_should_be(manage_labels.Labels)
            # Select a label.
            label = self.page.select_label(label_name)
            # Check if selected label
            if label is not None:
                # Delete the selected label.
                self.page.delete_label(label_name)
