from test.base_test import SuperAdminLoggedIn
from pages import homepage
from pages.page_objects.super_admin import manage_accounts, manage_account_users
from test.configuration import account_name, new_account_name, device,  user_id_link, user_name_link

import time

# TODO: To test whether the deleted account exist or not
# TODO: Adding a new user as Admin while linking a new user
# TODO: Changing the role of User to Admin or vice versa of existing user in any account


class ManageAccounts(SuperAdminLoggedIn):

    def setUp(self):
        super(ManageAccounts, self).setUp()
        self.page_should_be(homepage.HomePageSuperAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.manage_accounts)
        self.page_should_be(manage_accounts.Accounts)

# TODO: This method should have expected fail. As at the beginning, there will not be any account in the portal.
    # Test method to display the list of existing accounts in the portal.
    def test_accounts_list(self):
        self.page_should_be(manage_accounts.Accounts)
        time.sleep(2)
        self.driver.refresh()
        time.sleep(1)
        self.page_should_be(manage_accounts.Accounts)
        # Get list of all accounts in the portal.
        accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
        self.assertGreater(len(accounts), 0, "No account in the portal")

    # Test Method to first create an account then search it and at last delete it.
    def test_create_search_and_delete_account(self):
        self.page_should_be(manage_accounts.Accounts)
        # Create a new account.
        self.page.Buttons.create_account.click()
        self.page_should_be(manage_accounts.CreateAccount)

        try:
            self.page.create_new_account(account_name)
            time.sleep(3)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.Accounts)
            # Search for created account.
            self.page.search_account(account_name)
            # Get list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Verify the presence of created account in the portal.
            self.assertEqual(len(accounts), 1, "the number of accounts do not match")
            self.assertIn(account_name, accounts, "the account list doesn't contains that account")

        finally:
            self.driver.get(manage_accounts.MANAGE_ACCOUNTS_LIST)
            time.sleep(2)
            self.page_should_be(manage_accounts.Accounts)
            # Get list of accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Check if the account exists in accounts list , if yes, then process to delete that account.
            if account_name in accounts:
                # Select the searched account.
                self.page.select_account(account_name)
                time.sleep(2)
                self.page_should_be(manage_accounts.DeleteAccount)
                # Delete the selected account.
                self.page.delete_account()

    # Test method to link a user to an account and then unlink from the account.
    def test_link_and_unlink_user(self):
        self.page_should_be(manage_accounts.Accounts)
        # Create a new account.
        self.page.Buttons.create_account.click()
        self.page_should_be(manage_accounts.CreateAccount)

        try:
            self.page.create_new_account(account_name)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.Accounts)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(3)
            # To search for created account.
            self.page.search_account(account_name)
            # Getting list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Verify the presence of created account in the portal.
            self.assertEqual(len(accounts), 1, "the number of accounts do not match")
            self.assertIn(account_name, accounts, "the account list doesn't contains that account")

            # Select the searched account.
            self.page.select_account(account_name)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(3)
            # Link an user to the selected account.
            self.page.Buttons.account_link_user.click()
            self.page_should_be(manage_accounts.LinkUser)
            self.page.link_user(user_id_link)
            time.sleep(3)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.DeleteAccount)
            time.sleep(3)

            # Get list of all linked users to the account.
            linked_users = \
                [self.page.LinkedUserList.get_cell_from_row(r, 2).text for r in self.page.LinkedUserList.get_rows()]
            # Verify the presence of linked user in the account.
            self.assertEqual(len(linked_users), 1, "the number of users do not match")
            self.assertIn(user_id_link, linked_users, "the users list doesn't contains that user")

            # Open the profile of the linked user.
            self.page.select_user(user_id_link)
            # Unlink the user from that account(but that user will remain connected to that portal).
            self.page_should_be(manage_accounts.RemoveUser)
            self.page.remove_user()
            self.page_should_be(manage_account_users.UserList)
            time.sleep(3)
            # Click final ok to unlink the user from the account.
            # This method is present in manage_account_users.UserList.
            self.page.remove_user_ok()
            time.sleep(3)
            # After unlink the user, control will reach to Manage Account Users list page.
            self.page_should_be(manage_account_users.UserList)
            time.sleep(3)
            # Click Manage Accounts tab on Side Menu.
            self.page.SideMenu.click_to(self.page.SideMenu.manage_accounts)
            self.page_should_be(manage_accounts.Accounts)
            self.driver.refresh()
            time.sleep(3)
            # Select the searched account.
            self.page.select_account(account_name)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.DeleteAccount)

            # Get list of all linked users to the account.
            linked_users = \
                [self.page.LinkedUserList.get_cell_from_row(r, 1).text for r in self.page.LinkedUserList.get_rows()]
            # Verify the absence of unlinked user in the account.
            self.assertNotIn(user_name_link, linked_users, "the linked user list contains that user")

        finally:
            self.driver.get(manage_accounts.MANAGE_ACCOUNTS_LIST)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(2)
            self.page_should_be(manage_accounts.Accounts)

            # Get list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Check if the account exists in accounts list , if yes, then process to delete that account.
            if account_name in accounts:
                # Select the searched account.
                self.page.select_account(account_name)
                time.sleep(2)
                self.page_should_be(manage_accounts.DeleteAccount)
                # Delete the selected account.
                self.page.delete_account()

    # Test method to link a device to any account.
    def test_link_device(self):
        self.page_should_be(manage_accounts.Accounts)
        # Create a new account.
        self.page.Buttons.create_account.click()
        self.page_should_be(manage_accounts.CreateAccount)

        try:
            self.page.create_new_account(account_name)
            time.sleep(3)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.Accounts)
            # Search the recently created account.
            self.page.search_account(account_name)

            # Get list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Verify the presence of created account in the portal.
            self.assertEqual(len(accounts), 1, "the number of accounts do not match")
            self.assertIn(account_name, accounts, "the account list doesn't contains that account")

            # Select the searched account.
            self.page.select_account(account_name)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(3)
            # Link a device to the selected account.
            self.page.Buttons.account_link_device.click()
            self.page_should_be(manage_accounts.LinkDevice)
            self.page.link_device(device)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.DeleteAccount)
            time.sleep(2)

            # Get list of all linked devices to the account.
            linked_devices = \
                [self.page.LinkedDeviceList.get_cell_from_row(r, 3).text for r in self.page.LinkedDeviceList.get_rows()]
            # Verify the presence of linked device in the account.
            self.assertEqual(len(linked_devices), 1, "the number of devices do not match")
            self.assertIn(device, linked_devices, "the device list doesn't contains that device")
        finally:
            self.driver.get(manage_accounts.MANAGE_ACCOUNTS_LIST)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(2)
            self.page_should_be(manage_accounts.Accounts)
            # Get list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Check if the account exists in accounts list , if yes, then process to delete that account.
            if account_name in accounts:
                # Select the searched account.
                self.page.select_account(account_name)
                time.sleep(2)
                self.page_should_be(manage_accounts.DeleteAccount)
                # Delete the selected account.
                self.page.delete_account()

    # Test method to edit the account name.
    def test_edit_account_name(self):
        self.page_should_be(manage_accounts.Accounts)
        # Create a new account.
        self.page.Buttons.create_account.click()
        self.page_should_be(manage_accounts.CreateAccount)

        try:
            self.page.create_new_account(account_name)
            time.sleep(3)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.Accounts)
            # Search the created account.
            self.page.search_account(account_name)

            # Get list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Verify the presence of created account in the portal.
            self.assertEqual(len(accounts), 1, "the number of accounts do not match")
            self.assertIn(account_name, accounts, "the account list doesn't contains that account")

            # Select the searched account.
            self.page.select_account(account_name)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(manage_accounts.DeleteAccount)
            time.sleep(2)

            # Get account ID.
            elem_edit = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div/div[1]/div/div[1]')
            acct_id = elem_edit.text
            _, self.account_id = acct_id.split(": ")

            # Edit the name of the selected account.
            self.page.edit_account_name()
            self.page_should_be(manage_accounts.EditName)
            self.page.update_name(new_account_name)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(2)
            self.page_should_be(manage_accounts.DeleteAccount)

            # Verify the update of account name.
            elem = self.driver.find_element_by_xpath('//*[@id="tenant-name"]')
            self.assertIn(new_account_name, elem.text, "Account name not updated")

        finally:
            self.driver.get(manage_accounts.MANAGE_ACCOUNTS_LIST)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(2)
            self.page_should_be(manage_accounts.Accounts)
            # Get list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Check if the account is in accounts list but update of name failed, then process to delete the account.
            if account_name in accounts:
                self.page.select_account(account_name)
                time.sleep(2)
                self.page_should_be(manage_accounts.DeleteAccount)
                # Delete the select account.
                self.page.delete_account()

            self.driver.get(manage_accounts.MANAGE_ACCOUNTS_LIST)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(2)
            self.page_should_be(manage_accounts.Accounts)
            # Get list of all accounts.
            accounts = [self.page.AccountsList.get_cell_from_row(r, 1).text for r in self.page.AccountsList.get_rows()]
            # Check if the account is in the accounts list and update of name succeeded, process to delete the account.
            if new_account_name in accounts:
                # Select the searched account.
                self.page.select_account(new_account_name)
                time.sleep(2)
                self.page_should_be(manage_accounts.DeleteAccount)
                # Delete the selected account.
                self.page.delete_account()
