from test.base_test import SuperAdminLoggedIn
from pages import homepage
from pages.page_objects.super_admin import config_stripe_billing
from test.configuration import secret_key, publishable_key, invalid_secret_key, invalid_publishable_key

import time


class ConfigStripeBilling(SuperAdminLoggedIn):

    def setUp(self):
        super(ConfigStripeBilling, self).setUp()
        self.page_should_be(homepage.HomePageSuperAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.config_stripe_billing)
        self.page_should_be(config_stripe_billing.ConfigStripe)

    # Test method to save and delete new stripe configuration on/from the portal.
    def test_save_and_delete_stripe_config(self):
        self.page_should_be(config_stripe_billing.ConfigStripe)
        # Enter Stripe configuration.
        self.page.stripe_config(secret_key, publishable_key)
        time.sleep(1)
        self.page.Buttons.save_stripe_configuration.click()
        time.sleep(3)

        # Click final OK button to save the configuration.
        ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        ok.click()
        time.sleep(1)
        self.driver.refresh()
        time.sleep(1)

        # Delete the Stripe configuration.
        self.page.delete_stripe_config()
        time.sleep(1)

        # Re-enter the Stripe configuration to make the current portal run.
        if "test" not in self.driver.current_url:
            self.page_should_be(config_stripe_billing.ConfigStripe)
            self.page.stripe_config(secret_key, publishable_key)
            time.sleep(1)
            self.page.Buttons.save_stripe_configuration.click()
            time.sleep(3)

            # Click final OK button to save the configuration.
            ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
            ok.click()
            time.sleep(1)
            self.driver.refresh()
            time.sleep(1)

    # Test method to save and delete new stripe configuration on/from the portal.
    def test_save_invalid_stripe_config(self):
        self.page_should_be(config_stripe_billing.ConfigStripe)
        # Enter Stripe configuration.
        self.page.stripe_config(invalid_secret_key, publishable_key)
        time.sleep(1)
        self.page.Buttons.save_stripe_configuration.click()
        time.sleep(3)

        # Verify the status.
        elem = self.driver.find_element_by_xpath('//*[@id="err-msg"]')
        self.assertIn("Secret key is invalid", elem.text, "Portal able to save invalid Stripe configuration")

        time.sleep(3)
        self.driver.refresh()
        time.sleep(3)
        # Enter Stripe configuration.
        self.page.stripe_config(secret_key, invalid_publishable_key)
        time.sleep(1)
        self.page.Buttons.save_stripe_configuration.click()
        time.sleep(3)

        # Verify the status.
        elem = self.driver.find_element_by_xpath('//*[@id="err-msg"]')
        self.assertIn("Publishable key is invalid", elem.text, "Portal able to save invalid Stripe configuration")

        time.sleep(3)
        self.driver.refresh()
        time.sleep(3)

        # Re-enter the Stripe configuration to make the current portal run.
        if "test" not in self.driver.current_url:
            self.page_should_be(config_stripe_billing.ConfigStripe)
            self.page.stripe_config(secret_key, publishable_key)
            time.sleep(1)
            self.page.Buttons.save_stripe_configuration.click()
            time.sleep(3)

            # Click final OK button to save the configuration.
            ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
            ok.click()
            time.sleep(1)
            self.driver.refresh()
            time.sleep(1)
