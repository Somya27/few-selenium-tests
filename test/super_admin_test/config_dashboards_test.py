from test.base_test import SuperAdminLoggedIn
from pages import homepage
from pages.page_objects.super_admin import configure_dashboards as config_dashboards
from test.configuration import page_name

import time


class ConfigPage(SuperAdminLoggedIn):

    def setUp(self):
        super(ConfigPage, self).setUp()
        self.page_should_be(homepage.HomePageSuperAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.config_dashboards)
        self.page_should_be(config_dashboards.ConfigDashboardList)

    def test_add_dashboard_page(self):
        self.page_should_be(config_dashboards.ConfigDashboardList)
        self.page.Buttons.add_config_dashboard.click()
        time.sleep(2)
        self.page_should_be(config_dashboards.AddConfigDashboard)

        try:
            # Load original dashboard button click.
            elem = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div/div/div[1]/button[2]')
            elem.click()

            # Enter page name.
            elem = self.driver.find_element_by_xpath('//*[@id="page_name"]')
            elem.click()
            elem.clear()
            elem.send_keys(page_name)

            # Select Page Menu checkbox.
            elem = self.driver.find_element_by_xpath('//*[@id="page_menu"]')
            if not elem.is_selected():
                elem.click()

            self.assertEqual(elem.is_selected(), True, "Page menu is not selected")
            self.page.Buttons.save_and_activate_dashboard.click()

            time.sleep(1)
            # Yes, Continue!
            elem = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
            elem.click()

            time.sleep(1)
            # Ok
            elem = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
            elem.click()

            time.sleep(3)
            self.driver.refresh()
            time.sleep(3)
            self.page_should_be(config_dashboards.ConfigDashboardList)

            self.page.search_page(page_name)
            pages = [self.page.PageList.get_cell_from_row(r, 2).text for r in self.page.PageList.get_rows()]
            self.assertEqual(len(pages), 1, "the number of accounts do not match")
            self.assertIn(page_name, page_name, "the accounts list contains the account")

        finally:
            self.driver.get(config_dashboards.CONFIG_DASHBOARDS_LIST)
            time.sleep(2)
            self.page_should_be(config_dashboards.ConfigDashboardList)
            pages = [self.page.PageList.get_cell_from_row(r, 2).text for r in self.page.PageList.get_rows()]
            if page_name in pages:
                self.page.delete_page(page_name)

