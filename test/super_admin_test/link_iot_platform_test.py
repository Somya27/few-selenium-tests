from test.base_test import SuperAdminLoggedIn
from pages import homepage
from pages.page_objects.super_admin import link_iot_platform
from test.configuration import api_base_url, invalid_api_base_url, api_business_username, api_business_password, api_key

import time


class LinkIotPlatform(SuperAdminLoggedIn):

    def setUp(self):
        super(LinkIotPlatform, self).setUp()
        self.page_should_be(homepage.HomePageSuperAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.link_iot_platform)
        self.page_should_be(link_iot_platform.LinkIot)

    # Test method to link/unlink(disconnect) portal to/from platform.
    def test_link_platform(self):
        self.page_should_be(link_iot_platform.LinkIot)

        try:
            # Configure IoT credentials.
            self.page.link_configuration(api_base_url, api_business_username, api_business_password, api_key)
            time.sleep(1)

            # Test Connection.
            self.page.Buttons.test_connection.click()
            time.sleep(3)

            # Verify the status.
            elem = self.driver.find_element_by_xpath(
                '//*[@id="page-wrapper"]/div/div[2]/div/div/div/div[2]/form/fieldset[5]/label[1]')
            self.assertIn("Connected", elem.text, "Portal not able to connect")

            # Save link_platform configuration.
            self.page.Buttons.save_configuration.click()
            time.sleep(1)
            # Click final OK button to save the configuration.
            ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
            ok.click()
            time.sleep(1)
            self.driver.refresh()
            time.sleep(1)

        finally:
            self.driver.get(link_iot_platform.LINK_IOT_PLATFORM_CONFIG)
            time.sleep(2)
            self.driver.refresh()
            time.sleep(1)
            self.page_should_be(link_iot_platform.LinkIot)
            # Unlink portal from platform(delete link_platform configuration).
            self.page.delete_configuration()
            time.sleep(1)

            # Re-enter the platform configurations to make the current portal.
            if "test" not in self.driver.current_url:
                self.page_should_be(link_iot_platform.LinkIot)
                self.page.link_configuration(api_base_url, api_business_username, api_business_password, api_key)
                time.sleep(1)

                # Test Connection.
                self.page.Buttons.test_connection.click()
                time.sleep(3)

                # Verify the status.
                elem = self.driver.find_element_by_xpath(
                    '//*[@id="page-wrapper"]/div/div[2]/div/div/div/div[2]/form/fieldset[5]/label[1]')
                self.assertIn("Connected", elem.text, "Portal not able to connect")

                # Save link_platform configuration.
                self.page.Buttons.save_configuration.click()
                time.sleep(1)
                # Click final OK button to save the configuration.
                ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
                ok.click()
                time.sleep(1)
                self.driver.refresh()
                time.sleep(1)

    # Test method to link/unlink(disconnect) portal to/from platform with wrong credentials.
    def test_unable_link_platform(self):
        self.page_should_be(link_iot_platform.LinkIot)

        try:
            # Configure IoT credentials.
            self.page.link_configuration(invalid_api_base_url, api_business_username, api_business_password, api_key)
            time.sleep(1)
            # Test Connection.
            self.page.Buttons.test_connection.click()
            time.sleep(3)

            # Verify the status.
            elem = self.driver.find_element_by_xpath(
                '//*[@id="page-wrapper"]/div/div[2]/div/div/div/div[2]/form/fieldset[5]/label[1]')
            self.assertIn("Unable to Connect", elem.text, "Portal able to connect with wrong credential")

        finally:
            self.page_should_be(link_iot_platform.LinkIot)
            time.sleep(1)
            # Delete link_platform configuration.
            self.page.delete_configuration()
            time.sleep(1)

            # Re-enter the platform configurations to make the current portal.
            if "test" not in self.driver.current_url:
                self.page_should_be(link_iot_platform.LinkIot)
                self.page.link_configuration(api_base_url, api_business_username, api_business_password, api_key)
                time.sleep(1)

                # Test Connection.
                self.page.Buttons.test_connection.click()
                time.sleep(3)

                # Verify the status.
                elem = self.driver.find_element_by_xpath(
                    '//*[@id="page-wrapper"]/div/div[2]/div/div/div/div[2]/form/fieldset[5]/label[1]')
                self.assertIn("Connected", elem.text, "Portal not able to connect")

                # Save link_platform configuration.
                self.page.Buttons.save_configuration.click()
                time.sleep(1)
                # Click final OK button to save the configuration.
                ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
                ok.click()
                time.sleep(1)
                self.driver.refresh()
                time.sleep(1)
