from test.base_test import TenantUserLoggedIn
from pages import homepage
from pages.page_objects.tenant_user import tenant_user_device_summary

import time


class TuDeviceSummary(TenantUserLoggedIn):

    def setUp(self):
        super(TuDeviceSummary, self).setUp()
        self.page_should_be(homepage.HomePageTenantUser)
        self.page.SideMenu.click_to(self.page.SideMenu.tu_device_summary)
        self.page_should_be(tenant_user_device_summary.DeviceSummaryTenantUser)

    def test_device_summary_detail(self):
        self.page_should_be(tenant_user_device_summary.DeviceSummaryTenantUser)
        time.sleep(2)
        self.page.device_summary_tu()
        # Get device ID.
        elem_device = self.driver.find_element_by_xpath('//*[@id="device_id"]')
        self.device_id = elem_device.text
        self.page_should_be(tenant_user_device_summary.DeviceDetailTenantUser)
        time.sleep(3)




