from test.base_test import TenantAdminLoggedIn
from pages import homepage
from pages.page_objects.tenant_admin import tenant_admin_register_device
from test.configuration import device, new_device, device_err_msg_1, device_err_msg_2

import time


class TaRegisterDevice(TenantAdminLoggedIn):

    def setUp(self):
        super(TaRegisterDevice, self).setUp()
        self.page_should_be(homepage.HomePageTenantAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.register_device)
        self.page_should_be(tenant_admin_register_device.RegisterDevice)

    def test_add_exist_device_fail(self):
        self.page_should_be(tenant_admin_register_device.RegisterDevice)
        time.sleep(2)
        self.page.register_device(device)
        self.page.Buttons.ta_register_device.click()
        time.sleep(1)
        # Above submit should fail.
        elem = self.driver.find_element_by_xpath('//*[@id="step1MSG"]')
        self.assertEqual(device_err_msg_1, elem.text, "Register Device didn't fail")
        self.page_should_be(tenant_admin_register_device.RegisterDevice)

    def test_add_non_activated_device_fail(self):
        self.page_should_be(tenant_admin_register_device.RegisterDevice)
        time.sleep(2)
        self.page.register_device(new_device)
        self.page.Buttons.ta_register_device.click()
        time.sleep(1)
        # Above submit should fail.
        elem = self.driver.find_element_by_xpath('//*[@id="step1MSG"]')
        self.assertEqual(device_err_msg_2, elem.text, "Register Device didn't fail")
        self.page_should_be(tenant_admin_register_device.RegisterDevice)

