from test.base_test import TenantAdminLoggedIn
from pages import homepage
from pages.page_objects.tenant_admin import tenant_admin_device_summary
from test.configuration import new_place, old_place

import time


class TaDeviceSummary(TenantAdminLoggedIn):

    def setUp(self):
        super(TaDeviceSummary, self).setUp()
        self.page_should_be(homepage.HomePageTenantAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.ta_device_summary)
        self.page_should_be(tenant_admin_device_summary.DeviceSummaryTenantAdmin)

    def test_device_summary_detail(self):
        self.page_should_be(tenant_admin_device_summary.DeviceSummaryTenantAdmin)
        time.sleep(2)
        self.page.device_summary_ta()
        # Get device ID.
        elem_device = self.driver.find_element_by_xpath('//*[@id="device_id"]')
        self.device_id = elem_device.text
        self.page_should_be(tenant_admin_device_summary.DeviceDetailTenantAdmin)
        time.sleep(3)

    def test_edit_place(self):
        self.page_should_be(tenant_admin_device_summary.DeviceSummaryTenantAdmin)
        time.sleep(1)
        elem_device = self.driver.find_element_by_xpath('//*[@id="device_id"]')
        self.device_id = elem_device.text
        try:
            self.page.device_summary_ta()
            # Get device ID.

            self.page_should_be(tenant_admin_device_summary.DeviceDetailTenantAdmin)
            time.sleep(1)
            self.page.edit_place(new_place)
            time.sleep(1)
            self.page_should_be(tenant_admin_device_summary.DeviceDetailTenantAdmin)
            time.sleep(1)
            self.driver.get(tenant_admin_device_summary.TA_DEVICE_SUMMARY)
            time.sleep(1)
            self.page_should_be(tenant_admin_device_summary.DeviceSummaryTenantAdmin)
            time.sleep(1)
            self.driver.refresh()
            time.sleep(1)
            self.page_should_be(tenant_admin_device_summary.DeviceSummaryTenantAdmin)
            time.sleep(1)
            elem = self.driver.find_element_by_xpath(
                '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div[1]/div/div[2]/form/div/label/a')
            self.assertIn(new_place, elem.text, "Place name not updated")
        finally:
            self.driver.get(tenant_admin_device_summary.TA_DEVICE_DETAIL + "/" + self.device_id)
            time.sleep(1)
            self.page_should_be(tenant_admin_device_summary.DeviceDetailTenantAdmin)
            time.sleep(2)
            self.page.edit_place(old_place)
            time.sleep(1)
            self.page_should_be(tenant_admin_device_summary.DeviceDetailTenantAdmin)
            time.sleep(1)
            self.driver.get(tenant_admin_device_summary.TA_DEVICE_SUMMARY)
            time.sleep(1)
            self.page_should_be(tenant_admin_device_summary.DeviceSummaryTenantAdmin)
            time.sleep(1)


