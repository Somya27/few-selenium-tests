from test.base_test import TenantAdminLoggedIn
from pages import homepage
from pages.page_objects.tenant_admin import tenant_admin_manage_users
from test.configuration import ta_user_id_add, ta_user_name_add, ta_user_id, ta_user_name

import time


class TaManageUsers(TenantAdminLoggedIn):

    def setUp(self):
        super(TaManageUsers, self).setUp()
        self.page_should_be(homepage.HomePageTenantAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.ta_manage_users)
        # self.page_should_be(tenant_admin_manage_users.UsersListTa)

    def test_a_list_user(self):
        self.page_should_be(tenant_admin_manage_users.TaUsersList)
        # self.driver.get(tenant_admin_manage_users.TA_MANAGE_USERS_LIST)
        time.sleep(2)
        self.driver.refresh()
        time.sleep(1)
        self.page_should_be(tenant_admin_manage_users.TaUsersList)
        users = [self.page.UsersListTa.get_cell_from_row(r, 1).text for r in self.page.UsersListTa.get_rows()]
        self.assertGreater(len(users), 0, "the number of users do not match")

    def test_add_user(self):
        self.page_should_be(tenant_admin_manage_users.TaUsersList)
        self.page.Buttons.ta_add_new_user.click()
        self.page_should_be(tenant_admin_manage_users.TaAddUser)
        self.page.Buttons.ta_add_user.click()
        self.page.add_new_user(ta_user_id_add, ta_user_name_add)
        time.sleep(5)
        self.driver.refresh()
        time.sleep(3)
        self.page_should_be(tenant_admin_manage_users.TaUsersList)
        self.page.search_user(ta_user_id_add)
        users = [self.page.UsersListTa.get_cell_from_row(r, 1).text for r in self.page.UsersListTa.get_rows()]
        self.assertEqual(len(users), 1, "the number of users do not match")
        self.assertIn(ta_user_name_add, users, "the users list doesn't contain the user")

    def test_cancel_user(self):
        self.page_should_be(tenant_admin_manage_users.TaUsersList)
        self.page.Buttons.ta_add_new_user.click()
        self.page_should_be(tenant_admin_manage_users.TaAddUser)
        self.page.Buttons.ta_cancel_new_user.click()
        time.sleep(3)
        self.driver.refresh()
        time.sleep(3)
        self.page_should_be(tenant_admin_manage_users.TaUsersList)
        self.page.Buttons.ta_add_new_user.click()
        self.page_should_be(tenant_admin_manage_users.TaAddUser)
        # User already exist, add new should fail and stay on the same page.
        self.page.add_new_user(ta_user_id, ta_user_name)
        time.sleep(3)
        self.page_should_be(tenant_admin_manage_users.TaAddUser)
        self.page.Buttons.ta_cancel_new_user.click()
        time.sleep(3)
        self.driver.refresh()
        time.sleep(3)
        self.page_should_be(tenant_admin_manage_users.TaUsersList)

# TODO: To implement teat_remove_user method
