from test.base_test import TenantAdminLoggedIn
from pages import homepage
from pages.page_objects.tenant_admin import tenant_admin_manage_billing
from test.configuration import package1, package2, credit_card_detail, cvc, expire_month, expire_year, current_password

import time


class TaManageBilling(TenantAdminLoggedIn):

    def setUp(self):
        super(TaManageBilling, self).setUp()
        self.page_should_be(homepage.HomePageTenantAdmin)
        self.page.SideMenu.click_to(self.page.SideMenu.manage_billing)
        self.page_should_be(tenant_admin_manage_billing.ManageBilling)

    def test_add_cancel_subscription(self):
        self.page_should_be(tenant_admin_manage_billing.ManageBilling)
        time.sleep(2)
        self.page.Buttons.add_subscription.click()
        time.sleep(1)
        self.page.cancel_subscription(package1, current_password)
        time.sleep(3)
        self.page.Buttons.cancel_subscription.click()
        time.sleep(1)
        self.page_should_be(tenant_admin_manage_billing.ManageBilling)

    def test_add_paid_cancel_subscription(self):
        self.page_should_be(tenant_admin_manage_billing.ManageBilling)
        time.sleep(2)
        self.page.Buttons.add_subscription.click()
        time.sleep(1)
        self.page.invalid_subscription(package2, credit_card_detail, cvc, expire_month, expire_year, current_password)
        time.sleep(3)
        self.page.Buttons.cancel_subscription.click()
        time.sleep(1)
        self.page_should_be(tenant_admin_manage_billing.ManageBilling)



