class Buttons:
    def __init__(self, driver):
        self.driver = driver

    @property
    def create_account(self):
        button = self.driver.find_element_by_xpath('//*[@id="DataTables_Table_0"]/thead/tr[1]/th[3]/div/button')
        return button

    @property
    def delete_account(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div[3]/div/button')
        return button

    @property
    def create(self):
        button = self.driver.find_element_by_xpath('//*[@id="btn-create-tenant"]')
        return button

    @property
    def cancel(self):
        button = self.driver.find_element_by_xpath('//*[@id="tenant_form"]/div[2]/form/fieldset[2]/div[2]')
        return button

    # Using [1] and [2] with link_user's and link_device's xpath to differentiate as they both have same xpath
    @property
    def account_link_user(self):
        button = self.driver.find_element_by_xpath('(//*[@id="page"]/div/div[2]/div[1]/button)[1]')
        return button

    @property
    def link_link_user(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_link"]/div[2]/form/fieldset[3]/div[1]')
        return button

    @property
    def cancel_link_user(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_link"]/div[2]/form/fieldset[3]/div[2]')
        return button

    @property
    def remove_user(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form"]/div/form/fieldset[13]/div/button[4]')
        return button

    @property
    def account_link_device(self):
        button = self.driver.find_element_by_xpath('(//*[@id="page"]/div/div[2]/div[1]/button)[2]')
        return button

    @property
    def link_link_device(self):
        button = self.driver.find_element_by_xpath('//*[@id="device_form"]/div[2]/form/fieldset[2]/div[1]')
        return button

    @property
    def cancel_link_device(self):
        button = self.driver.find_element_by_xpath('//*[@id="device_form"]/div[2]/form/fieldset[2]/div[2]')
        return button

    @property
    def edit_account_name(self):
        button = self.driver.find_element_by_xpath('//*[@id="tenant-name"]/button[1]')
        return button

    @property
    def update_account_name(self):
        button = self.driver.find_element_by_xpath('//*[@id="btn-update-tenant"]')
        return button

    @property
    def switch_to_account_view(self):
        button = self.driver.find_element_by_xpath('//*[@id="tenant-name"]/button[2]')
        return button

    @property
    def invite(self):
        button = self.driver.find_element_by_xpath('//button[@class="btn btn-labeled btn-success"]')
        # id of the following xpath changes every time on loading so using using above class xpath
        # button = self.driver.find_element_by_xpath('//*[@id="DataTables_Table_68"]/thead/tr[1]/th[3]/div/button')
        return button

    @property
    def invite_invite(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_add"]/div[2]/form/fieldset[3]/input[1]')
        return button

    @property
    def cancel_invite(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_add"]/div[2]/form/fieldset[3]/input[2]')
        return button

    @property
    def delete_user(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form"]/div/form/fieldset[13]/div/button[3]')
        return button

    @property
    def save_stripe_configuration(self):
        button = self.driver.find_element_by_xpath('//*[@id="btn-save"]')
        return button

    @property
    def delete_stripe_configuration(self):
        button = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div[2]/div[1]/div')
        return button

    @property
    def add_device(self):
        button = self.driver.find_element_by_xpath('//*[@id="DataTables_Table_7"]/thead/tr[1]/th[4]/div/button')
        return button

    @property
    def create_label(self):
        button = self.driver.find_element_by_xpath('//*[@id="DataTables_Table_0"]/thead/tr[1]/th[3]/div/button')
        return button

    @property
    def delete_label(self):
        button = self.driver.find_element_by_xpath('//*[@id="DataTables_Table_9"]/tbody/tr[1]/td[3]/input')
        return button

    @property
    def save_label(self):
        button = self.driver.find_element_by_xpath('//*[@id="label_form_add"]/div[2]/form/fieldset[3]/input[1]')
        return button

    @property
    def cancel_label(self):
        button = self.driver.find_element_by_xpath('//*[@id="label_form_add"]/div[2]/form/fieldset[3]/input[2]')
        return button

    @property
    def test_connection(self):
        button = self.driver.find_element_by_xpath('//*[@id="test-connection"]')
        return button

    @property
    def save_configuration(self):
        button = self.driver.find_element_by_xpath('//*[@id="btn-save"]')
        return button

    @property
    def delete_configuration_and_disconnect(self):
        button = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div[2]/div/div/div/div[1]/input')
        return button

    @property
    def add_config_dashboard(self):
        button = self.driver.find_element_by_xpath('//*[@id="table"]/thead/tr[1]/th[4]/div/button')
        return button

    @property
    def delete_config_dashboard(self):
        button = self.driver.find_element_by_xpath('//*[@id="table"]/tbody/tr[1]/td[4]/input')
        return button

    @property
    def load_last_saved_dashboard(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div/div/div[1]/button[1]')
        return button

    @property
    def load_original_dashboard(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div/div/div[1]/button[2]')
        return button

    @property
    def export_html_to_file(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div/div/div[3]/div/button')
        return button

    @property
    def export_javascript_to_file(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div/div/div[5]/div/button')
        return button

    @property
    def save_and_activate_dashboard(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div/div/div[7]/button[1]')
        return button

    @property
    def cancel_dashboard(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div/div/div[7]/button[2]')
        return button

    @property
    def load_original_logo_url(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div[2]/div/div/div/div/form/fieldset[3]/div/div[2]')
        return button

    @property
    def load_original_background_url(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div[2]/div/div/div/div/form/fieldset[4]/div/div[2]')
        return button

    @property
    def save_config_portal_details(self):
        button = self.driver.find_element_by_xpath('//*[@id="btn-save"]')
        return button

    @property
    def place_edit(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div/div[2]/div/div/div/div/div[1]/div/button')
        return button

    @property
    def place_save(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div/div[2]/div/div/div/div/div[1]/div/button[1]')
        return button

    @property
    def place_cancel(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div/div[2]/div/div/div/div/div[1]/div/button[2]')
        return button

    @property
    def add_alert(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div/div[2]/div/div/div/div/div[10]/alert-config/div/div'
            '/div/div/div/div[3]/button')
        return button

    @property
    def unregister_device(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div/div/div[2]/div/div/div/div/div[1]/button')
        return button

    @property
    def add_manage_users(self):
        button = self.driver.find_element_by_xpath('//*[@id="DataTables_Table_1"]/thead/tr[1]/th[3]/div/button')
        return button

    @property
    def add_add_manage_users(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_add_or_link"]/div/form/fieldset[4]/div[1]')
        return button

    @property
    def cancel_manage_users(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_add_or_link"]/div/form/fieldset[4]/div[2]')
        return button

    @property
    def submit(self):
        button = self.driver.find_element_by_xpath('//*[@id="btn-save"]')
        return button

    # Xpath is not correct for add_subscription
    @property
    def add_subscription(self):
        button = self.driver.find_element_by_xpath('//*[@id="NgCtrlTag"]/div[2]/div[1]/div/div')
        return button

    @property
    def ok_subscription(self):
        button = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[3]/div/button[2]')
        return button

    @property
    def cancel_subscription(self):
        button = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[3]/div/button[1]')
        return button

    @property
    def unsubscribe(self):
        button = self.driver.find_element_by_xpath('//*[@id="NgCtrlTag"]/div[2]/div[1]/div/div')
        return button

    @property
    def billing_history(self):
        button = self.driver.find_element_by_xpath('//*[@id="NgCtrlTag"]/div[2]/div[2]/div/div/div[7]/label[1]')
        return button

    @property
    def payment_history(self):
        button = self.driver.find_element_by_xpath('//*[@id="NgCtrlTag"]/div[2]/div[2]/div/div/div[7]/label[2]')
        return button

    @property
    def update_credit_card(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="NgCtrlTag"]/div[2]/div[2]/div/div/div[9]/div[1]/button')
        return button

    @property
    def change_subscription(self):
        button = self.driver.find_element_by_xpath(
            '//*[@id="NgCtrlTag"]/div[2]/div[2]/div/div/div[9]/div[2]/button')
        return button

    @property
    def ok_update_credit_card(self):
        button = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[4]/div/button[2]')
        return button

    @property
    def cancel_update_credit_card(self):
        button = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[4]/div/button[1]')
        return button

    @property
    def ok_change_subscription(self):
        button = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[3]/div/button[2]')
        return button

    @property
    def cancel_change_subscription(self):
        button = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[3]/div/button[1]')
        return button

    @property
    def ta_add_new_user(self):
        button = self.driver.find_element_by_xpath('//button[@class="btn btn-labeled btn-success"]')
        return button

    @property
    def ta_add_user(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_add_or_link"]/div/form/fieldset[4]/div[1]')
        return button

    @property
    def ta_cancel_new_user(self):
        button = self.driver.find_element_by_xpath('//*[@id="user_form_add_or_link"]/div/form/fieldset[4]/div[2]')
        return button

    @property
    def ta_register_device(self):
        button = self.driver.find_element_by_xpath('//*[@id="btn-save"]')
        return button
