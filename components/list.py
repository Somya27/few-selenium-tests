
# Table/List
TABLE = '//*[@id="{}"]'


class List:
    def __init__(self, driver, id):
        self.driver = driver
        self.TABLE = TABLE.format(id)

    def get_headers(self):
        headers = self.driver.find_elements_by_xpath("{}//thead/tr/th".format(self.TABLE))
        return [h.text for h in headers]

    def get_rows(self):
        rows = self.driver.find_elements_by_xpath("{}//tbody/tr".format(self.TABLE))
        return rows

    def get_row_by_index(self, index):
        row = self.driver.find_element_by_xpath("{}//tbody/tr[{}]".format(self.TABLE, index))
        return row

    def get_row_by_content(self, column, value):
        row = self.driver.find_element_by_xpath(
            "{}//tbody/tr/td[{}]//*[text()='{}']/ancestor::tr".format(self.TABLE, column, value))
        return row

    def get_row_index_by_content(self, column, value):
        rows = self.driver.find_elements_by_xpath("{}//tbody/tr".format(self.TABLE))
        row = self.driver.find_element_by_xpath(
            "{}//tbody/tr/td[{}]//span[text()='{}']/ancestor::tr".format(self.TABLE, column, value))
        return rows.index(row)

    def get_cell(self, row, column):
        cell = self.driver.find_element_by_xpath("{}//tbody/tr[{}]/td[{}]".format(self.TABLE, row, column))
        return cell

    @staticmethod
    def get_cell_from_row(row, column):
        # get relative path to td with .//td
        cell = row.find_element_by_xpath(".//td[{}]".format(column))
        return cell
