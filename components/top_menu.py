# from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from test.configuration import *

import time

# Top Menu
MENU_ID = '//*[@id="side-menu"]'
ADMIN_MENU_ID = "{}/li[1]/div[1]".format(MENU_ID)
ADMIN_MENU_LINK = "{}/a/span[2]".format(ADMIN_MENU_ID)
USER_MENU_ID = "{}/li[2]".format(MENU_ID)
USER_MENU_LINK = "{}/a/span[2]".format(USER_MENU_ID)

# Logout
ADMIN_LOGOUT_MENU = "{}/ul/li[3]".format(ADMIN_MENU_ID)
ADMIN_LOGOUT_BUTTON = "{}/ul/li[3]/a".format(ADMIN_MENU_ID)
USER_LOGOUT_MENU = "{}/ul/li[3]".format(USER_MENU_ID)
USER_LOGOUT_BUTTON = "{}/ul/li[3]/a".format(USER_MENU_ID)

# Profile
ADMIN_PROFILE_MENU = "{}/ul/li[1]".format(ADMIN_MENU_ID)
ADMIN_PROFILE_BUTTON = "{}/ul/li[1]/a".format(ADMIN_MENU_ID)


class TopMenu:
    def __init__(self, driver):
        self.driver = driver

    def logout(self):
        wait = WebDriverWait(self.driver, MID_WAIT)
        elem = self.driver.find_element_by_xpath(ADMIN_MENU_LINK)
        time.sleep(1)
        elem.click()
        time.sleep(3)
        wait.until(EC.visibility_of_element_located((By.XPATH, ADMIN_LOGOUT_MENU)))
        elem = self.driver.find_element_by_xpath(ADMIN_LOGOUT_BUTTON)
        elem.click()

    def profile(self):
        wait = WebDriverWait(self.driver, MID_WAIT)
        elem = self.driver.find_element_by_xpath(ADMIN_MENU_LINK)
        time.sleep(1)
        elem.click()
        time.sleep(3)
        wait.until(EC.visibility_of_element_located((By.XPATH, ADMIN_PROFILE_MENU)))
        elem = self.driver.find_element_by_xpath(ADMIN_PROFILE_BUTTON)
        elem.click()

