from selenium.webdriver.common.action_chains import ActionChains


# Side Menu
MENU_ID = '//*[@id="side-menu"]'

# Menu description

# Account
MANAGE_ACCOUNTS = {'menu': "Manage Accounts", 'url': "web/#/app/sa_manage_tenants"}

MANAGE_CREATE_ACCOUNT = {'menu': "Create Account", 'url': "web/#/app/add_form/Tenants"}

MANAGE_DELETE_ACCOUNT = {'menu': "Delete Account", 'url': "web/#/app/sa_single_tenant"}

MANAGE_LINK_USER = {'menu': "Link User", 'url': "web/#/app/add_form/Users_link"}

MANAGE_REMOVE_USER = {'menu': "Link User", 'url': "web/#/app/view_user"}

MANAGE_LINK_DEVICE = {'menu': "Link Device", 'url': "web/#/app/add_form/Devices"}

MANAGE_EDIT_NAME = {'menu': "Edit Name", 'url': "web/#/app/edit_form/Tenant"}

# User
MANAGE_ACCOUNT_USERS = {'menu': "Manage Account Users", 'url': "web/#/app/sa_manage_users"}

INVITE_USERS = {'menu': "Invite Account User", 'url': "web/#/app/add_form/Users_add"}

DELETE_USER = {'menu': "User", 'url': "web/#/app/view_user"}

# Device
MANAGE_DEVICES = {'menu': "Manage Devices", 'url': "web/#/app/sa_manage_devices"}

ADD_DEVICE = {'menu': "Add Device", 'url': "web/#/app/add_form/Devices_add"}

# Label
MANAGE_LABELS = {'menu': "Manage Labels", 'url': "web/#/app/sa_manage_labels"}

CREATE_LABEL = {'menu': "Create Label", 'url': "web/#/app/add_form/labels"}

# Link Iot Platform
LINK_IOT_PLATFORM = {'menu': "Link IoT Platform", 'url': "web/#/app/sa_m1platform_config"}

# Configure Dashboard
CONFIG_DASHBOARDS = {'menu': "Config Dashboards", 'url': "web/#/app/sa_manage_dashboard/list"}

CONFIG_DASHBOARDS_ADD = {'menu': "Config Dashboards", 'url': "web/#/app/sa_manage_dashboard/edit"}

# Configure Portal Details
CONFIG_PORTAL_DETAILS = {'menu': "Config Portal Details", 'url': "web/#/app/sa_main_config"}

# Configure Stripe Billing
CONFIG_STRIPE_BILLING = {'menu': "Config Stripe Billing", 'url': "web/#/app/sa_manage_stripe"}

# User as Admin
TA_MANAGE_USERS = {'menu': "Manage Users", 'url': "web/#/app/manage_users"}

TA_MANAGE_USERS_ADD = {'menu': "Manage Users", 'url': "web/#/app/add_form/Users_link_or_add"}

# Device
DEVICE_SUMMARY = {'menu': "Device Summary", 'url': "web/#/app/pages/devicesummary"}

DEVICE_DETAIL = {'menu': "Device Summary", 'url': "web/#/app/pages/devicedetail"}

REGISTER_DEVICE = {'menu': "Register Device", 'url': "web/#/app/register_device"}

# Manage Billing on User as Admin Portal
MANAGE_BILLING = {'menu': "Manage Billing", 'url': "web/#/app/billing"}


def navigate_to(self, menu):
    sidebar_menu = self.driver.find_element_by_xpath(MENU_ID)
    ActionChains(self.driver).move_to_element(sidebar_menu).perform()
    elem = self.driver.find_element_by_xpath("{}//span[text()='{}']/ancestor::li".format(MENU_ID, menu['menu']))
    # check if the menu is active, if so just move to, else click to open
    active = "active" in elem.get_attribute("class")
    if active:
        ActionChains(self.driver).move_to_element(elem).perform()
    else:
        ActionChains(self.driver).move_to_element(elem).click().perform()


class Admin:
    def __init__(self, driver):
        self.driver = driver

    def click_to(self, menu):
        navigate_to(self, menu)

    @property
    def manage_accounts(self):
        return MANAGE_ACCOUNTS

    @property
    def manage_create_account(self):
        return MANAGE_CREATE_ACCOUNT

    @property
    def manage_delete_account(self):
        return MANAGE_DELETE_ACCOUNT

    @property
    def manage_link_user(self):
        return MANAGE_LINK_USER

    @property
    def manage_remove_user(self):
        return MANAGE_REMOVE_USER

    @property
    def manage_link_device(self):
        return MANAGE_LINK_DEVICE

    @property
    def manage_edit_name(self):
        return MANAGE_EDIT_NAME

    @property
    def manage_account_users(self):
        return MANAGE_ACCOUNT_USERS

    @property
    def invite_users(self):
        return INVITE_USERS

    @property
    def delete_user(self):
        return DELETE_USER

    @property
    def manage_devices(self):
        return MANAGE_DEVICES

    @property
    def add_device(self):
        return ADD_DEVICE

    @property
    def manage_labels(self):
        return MANAGE_LABELS

    @property
    def create_label(self):
        return CREATE_LABEL

    @property
    def config_stripe_billing(self):
        return CONFIG_STRIPE_BILLING

    @property
    def link_iot_platform(self):
        return LINK_IOT_PLATFORM

    @property
    def config_dashboards(self):
        return CONFIG_DASHBOARDS

    @property
    def config_dashboards_add(self):
        return CONFIG_DASHBOARDS_ADD

    @property
    def config_portal_details(self):
        return CONFIG_PORTAL_DETAILS


class TenantAsAdmin:
    def __init__(self, driver):
            self.driver = driver

    def click_to(self, menu):
        navigate_to(self, menu)

    @property
    def ta_device_summary(self):
        return DEVICE_SUMMARY

    @property
    def ta_device_detail(self):
        return DEVICE_DETAIL

    @property
    def ta_manage_users(self):
        return TA_MANAGE_USERS

    @property
    def ta_manage_users_add(self):
        return TA_MANAGE_USERS_ADD

    @property
    def register_device(self):
        return REGISTER_DEVICE

    @property
    def manage_billing(self):
        return MANAGE_BILLING


class TenantAsUser:

    def __init__(self, driver):
            self.driver = driver

    def click_to(self, menu):
        navigate_to(self, menu)

    @property
    def tu_device_summary(self):
        return DEVICE_SUMMARY

    @property
    def tu_device_detail(self):
        return DEVICE_DETAIL
