from pages.base_page import BaseSuperAdminPage, BaseTenantAdminPage, BaseTenantUserPage
from test.configuration import BASE_URL
from selenium.common.exceptions import NoSuchElementException


DASHBOARD_URL = BASE_URL + "/#/dashboard"


class HomePageSuperAdmin(BaseSuperAdminPage):
    def __init__(self, test):
        super(HomePageSuperAdmin, self).__init__(test)

    def verify_page(self):
        super(HomePageSuperAdmin, self).verify_page()
        elem_heading = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div[1]/div/h2')
        return elem_heading.text == "Manage Accounts"


class HomePageTenantAdmin(BaseTenantAdminPage):
    def __init__(self, test):
        super(HomePageTenantAdmin, self).__init__(test)

    def verify_page(self):
        super(HomePageTenantAdmin, self).verify_page()
        try:
            elem_heading = self.driver.find_element_by_xpath('//*[@id="NgCtrlTag"]/div[1]/div/h2')
            return elem_heading.text == "Manage Billing"
        except NoSuchElementException:
            print("Manage Billing page not found")

        # Move forward.
        elem_heading = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div/div[1]/div/h2')
        return elem_heading.text == "Device Summary"


class HomePageTenantUser(BaseTenantUserPage):
    def __init__(self, test):
        super(HomePageTenantUser, self).__init__(test)

    def verify_page(self):
        super(HomePageTenantUser, self).verify_page()
        elem_heading = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div/div[1]/div/h2')
        return elem_heading.text == "Device Summary"


