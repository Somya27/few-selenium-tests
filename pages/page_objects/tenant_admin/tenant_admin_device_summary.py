from pages.base_page import BaseTenantAdminPage
from test.configuration import BASE_URL
from components import buttons, side_menu


TA_DEVICE_SUMMARY = BASE_URL + "/" + side_menu.TenantAsAdmin(None).ta_device_summary['url']
TA_DEVICE_DETAIL = BASE_URL + "/" + side_menu.TenantAsAdmin(None).ta_device_detail['url']


class DeviceSummaryTenantAdmin(BaseTenantAdminPage):
    def __init__(self, test):
        super(DeviceSummaryTenantAdmin, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(DeviceSummaryTenantAdmin, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TA_DEVICE_SUMMARY)
        return True

    def device_summary_ta(self):
        elem = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div[1]')
        elem.click()


class DeviceDetailTenantAdmin(BaseTenantAdminPage):
    def __init__(self, test):
        super(DeviceDetailTenantAdmin, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(DeviceDetailTenantAdmin, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TA_DEVICE_DETAIL + "/" + self.test.device_id)
        return True

    def edit_place(self, new_place):
        self.Buttons.place_edit.click()
        elem = self.driver.find_element_by_xpath('//*[@id="place"]')
        elem.clear()
        elem.click()
        elem.send_keys(new_place)
        self.Buttons.place_save.click()









