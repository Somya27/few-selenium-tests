from pages.base_page import BaseTenantAdminPage
from test.configuration import BASE_URL
from components import buttons, side_menu

import time

TA_REGISTER_DEVICE_URL = BASE_URL + "/" + side_menu.TenantAsAdmin(None).register_device['url']


class RegisterDevice(BaseTenantAdminPage):

    def __init__(self, test):
        super(RegisterDevice, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(RegisterDevice, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TA_REGISTER_DEVICE_URL)
        return True

    def register_device(self, device):
        elem = self.driver.find_element_by_xpath(
            '//*[@id="page-wrapper"]/div/div[2]/div/div/div/div/div[2]/div/form/fieldset[1]/div/div/input')
        elem.clear()
        elem.click()
        elem.send_keys(device)
        time.sleep(2)