from pages.base_page import BaseTenantAdminPage
from test.configuration import BASE_URL
from components import buttons, side_menu

import time

TA_MANAGE_BILLING = BASE_URL + "/" + side_menu.TenantAsAdmin(None).manage_billing['url']


class ManageBilling(BaseTenantAdminPage):
    def __init__(self, test):
        super(ManageBilling, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(ManageBilling, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TA_MANAGE_BILLING)
        return True

    def invalid_subscription(self, package, credit_card_detail, cvc, expire_month, expire_year, current_password):
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[1]/div/h4/select')
        for option in elem.find_elements_by_tag_name('option'):
            if option.text == package:
                option.click()
                break
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[2]/div[2]/div[2]/input')
        elem.click()
        elem.send_keys(credit_card_detail)
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[2]/div[3]/div[2]/input')
        elem.click()
        elem.send_keys(cvc)
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[2]/div[4]/div[2]/input')
        elem.click()
        elem.send_keys(expire_month)
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[2]/div[4]/div[4]/input')
        elem.click()
        elem.send_keys(expire_year)
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[3]/div[2]/input')
        elem.click()
        elem.send_keys(current_password)

    def cancel_subscription(self, package1, current_password):
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[1]/div/h4/select')
        elem.click()
        elem.send_keys(package1)
        elem = self.driver.find_element_by_xpath('//*[@id="card_form"]/div[2]/div/div[2]/div[2]/input')
        elem.click()
        elem.send_keys(current_password)

