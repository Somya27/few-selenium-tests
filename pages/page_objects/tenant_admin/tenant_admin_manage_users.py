from pages.base_page import BaseTenantAdminPage
from test.configuration import BASE_URL
from components import buttons, list,  side_menu

import time

TA_MANAGE_USERS_LIST = BASE_URL + "/" + side_menu.TenantAsAdmin(None).ta_manage_users['url']
TA_MANAGE_USERS_ADD = BASE_URL + "/" + side_menu.TenantAsAdmin(None).ta_manage_users_add['url']


class TaUsersList(BaseTenantAdminPage):
    def __init__(self, test):
        super(TaUsersList, self).__init__(test)
        self.ListWidget = list.List(self.driver, id="DataTables_Table_0")
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(TaUsersList, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TA_MANAGE_USERS_LIST)
        return True

    def select_user(self, user_name):
        self.search_user(user_name)
        user = self.ListWidget.get_row_by_index(1)

        if user is not None:
            user.click()
            time.sleep(0.5)

    def search_user(self, search_filter):
        elem = self.driver.find_element_by_xpath('//input[@class="form-control input-sm"]')
        elem.clear()
        elem.click()
        elem.send_keys(search_filter)
        time.sleep(1)


class TaAddUser(BaseTenantAdminPage):
    def __init__(self, test):
        super(TaAddUser, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(TaAddUser, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TA_MANAGE_USERS_ADD)
        return True

    def add_new_user(self, ta_user_id, ta_user_name):
        elem = self.driver.find_element_by_xpath('//*[@id="login-input-user-al"]')
        elem.send_keys(ta_user_id)
        elem = self.driver.find_element_by_xpath('//*[@id="name-input-user-al"]')
        elem.send_keys(ta_user_name)
        elem = self.driver.find_element_by_xpath('//*[@id="select-user-al"]')
        elem.click()
        time.sleep(1)
        self.Buttons.ta_add_user.click()

