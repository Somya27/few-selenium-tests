from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL
from components import buttons, side_menu

import time

CONFIG_PORTAL_DETAILS = BASE_URL + "/" + side_menu.Admin(None).config_portal_details['url']


class ConfigPortalDetails(BaseSuperAdminPage):
    def __init__(self, test):
        super(ConfigPortalDetails, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(ConfigPortalDetails, self).verify_page()
        self.test.assertEqual(self.driver.current_url, CONFIG_PORTAL_DETAILS)
        return True

    # Method to load the original(Medium One) logo and background.
    def load_original(self):
        self.Buttons.load_original_logo_url.click()
        self.Buttons.load_original_background_url.click()
        elem = self.driver.find_element_by_xpath('//*[@id="logo-input"]')
        logo = elem.get_attribute("value")
        elem = self.driver.find_element_by_xpath('//*[@id="bg-input"]')
        background = elem.get_attribute("value")
        time.sleep(2)
        self.Buttons.save_config_portal_details.click()
        time.sleep(2)

        # Click final OK button.
        ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        ok.click()
        time.sleep(2)
        return logo, background

    # Method to configure the portal with Sensorfeed logo and background.
    def configure(self, portal_name, vendor_url, logo_url, background_url, favicon_url):
        elem = self.driver.find_element_by_xpath('//*[@id="name-input"]')
        elem.clear()
        elem.click()
        elem.send_keys(portal_name)
        elem = self.driver.find_element_by_xpath('//*[@id="vendor-url-input"]')
        elem.clear()
        elem.click()
        elem.send_keys(vendor_url)
        elem = self.driver.find_element_by_xpath('//*[@id="logo-input"]')
        elem.clear()
        elem.click()
        elem.send_keys(logo_url)
        elem = self.driver.find_element_by_xpath('//*[@id="bg-input"]')
        elem.clear()
        elem.click()
        elem.send_keys(background_url)
        elem = self.driver.find_element_by_xpath('//*[@id="favicon-input"]')
        elem.clear()
        elem.click()
        elem.send_keys(favicon_url)
        time.sleep(2)
        self.Buttons.save_config_portal_details.click()
        time.sleep(2)

        # Click final OK button.
        ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        ok.click()
        time.sleep(2)
