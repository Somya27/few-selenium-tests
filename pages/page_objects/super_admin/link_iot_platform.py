from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL
from components import buttons, side_menu

import time

LINK_IOT_PLATFORM_CONFIG = BASE_URL + "/" + side_menu.Admin(None).link_iot_platform['url']


class LinkIot(BaseSuperAdminPage):
    def __init__(self, test):
        super(LinkIot, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(LinkIot, self).verify_page()
        self.test.assertEqual(self.driver.current_url, LINK_IOT_PLATFORM_CONFIG)
        return True

    # Method to save platform configuration and link portal to platform.
    def link_configuration(self, api_base_url, api_business_username, api_business_password, api_key):
        elem = self.driver.find_element_by_xpath('//*[@id="base-url"]')
        elem.clear()
        elem.click()
        elem.send_keys(api_base_url)
        elem = self.driver.find_element_by_xpath('//*[@id="business-usr"]')
        elem.clear()
        elem.click()
        elem.send_keys(api_business_username)
        elem = self.driver.find_element_by_xpath('//*[@id="business-pwd"]')
        elem.clear()
        elem.click()
        elem.send_keys(api_business_password)
        elem = self.driver.find_element_by_xpath('//*[@id="api-key"]')
        elem.clear()
        elem.click()
        elem.send_keys(api_key)

    # Method to delete platform configuration and disconnect portal from platform.
    def delete_configuration(self):
        self.Buttons.delete_configuration_and_disconnect.click()
        time.sleep(1)

        # Yes, Delete! button.
        yes_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        yes_delete.click()
        time.sleep(1)

        # Enter DELETE to confirm delete.
        type_delete = self.driver.find_element_by_xpath('/html/body/div[4]/fieldset/input')
        type_delete.clear()
        type_delete.click()
        type_delete.send_keys('DELETE')
        time.sleep(1)

        # Click delete.
        final_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        final_delete.click()
        time.sleep(1)

        # Click final OK button.
        ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        ok.click()
        time.sleep(1)
