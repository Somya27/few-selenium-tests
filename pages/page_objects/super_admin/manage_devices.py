from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL
from components import buttons, list, side_menu


MANAGE_DEVICES_LIST = BASE_URL + "/" + side_menu.Admin(None).manage_devices['url']
ADD_DEVICE = BASE_URL + "/" + side_menu.Admin(None).add_device['url']


class DeviceList(BaseSuperAdminPage):
    def __init__(self, test):
        super(DeviceList, self).__init__(test)
        self.ListWidget = list.List(self.driver, id="")
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(DeviceList, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_DEVICES_LIST)
        return True


class AddDevice(BaseSuperAdminPage):
    def __init__(self, test):
        super(AddDevice, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(AddDevice, self).verify_page()
        self.test.asserEqual(self.driver.current_url, ADD_DEVICE)
        return True

    def add_new_device(self, new_device):
        # TODO: Implement this method, when portal get fixed.
        return None
