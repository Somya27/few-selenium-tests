from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL, user_id_link
from components import buttons, list, side_menu

import time

MANAGE_ACCOUNTS_LIST = BASE_URL + "/" + side_menu.Admin(None).manage_accounts['url']
MANAGE_ACCOUNTS_CREATE = BASE_URL + "/" + side_menu.Admin(None).manage_create_account['url']
MANAGE_ACCOUNTS_DELETE = BASE_URL + "/" + side_menu.Admin(None).manage_delete_account['url']
MANAGE_ACCOUNTS_LINK_USER = BASE_URL + "/" + side_menu.Admin(None).manage_link_user['url']
MANAGE_ACCOUNTS_REMOVE_USER = BASE_URL + "/" + side_menu.Admin(None).manage_remove_user['url']
MANAGE_ACCOUNTS_LINK_DEVICE = BASE_URL + "/" + side_menu.Admin(None).manage_link_device['url']
MANAGE_ACCOUNTS_EDIT_NAME = BASE_URL + "/" + side_menu.Admin(None).manage_edit_name['url']
MANAGE_ACCOUNT_USERS_LIST = BASE_URL + "/" + side_menu.Admin(None).manage_account_users['url']


class Accounts(BaseSuperAdminPage):
    def __init__(self, test):
        super(Accounts, self).__init__(test)
        # Define what components we have on the page.
        self.AccountsList = list.List(self.driver, id="DataTables_Table_0")
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(Accounts, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNTS_LIST)
        return True

    # Method to search any account.
    def search_account(self, search_filter):
        elem = self.driver.find_element_by_xpath('//input[@class="form-control input-sm"]')
        elem.clear()
        elem.click()
        elem.send_keys(search_filter)
        time.sleep(1)

    # Method to select the searched account.
    def select_account(self, account_name):
        self.search_account(account_name)
        account = self.AccountsList.get_row_by_index(1)

        if account is not None:
            account.click()
            time.sleep(3)


class CreateAccount(BaseSuperAdminPage):
    def __init__(self, test):
        super(CreateAccount, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(CreateAccount, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNTS_CREATE)
        return True

    # Method to create a new account.
    def create_new_account(self, account_name):
        elem = self.driver.find_element_by_xpath('//*[@id="input-tenant-name"]')
        elem.send_keys(account_name)
        self.Buttons.create.click()

    # Method to cancel while creating a new account(after entering any account name).
    def cancel_new_account(self, account_name):
        elem = self.driver.find_element_by_xpath('//*[@id="input-tenant-name"]')
        elem.send_keys(account_name)
        time.sleep(1)
        self.Buttons.cancel.click()


class DeleteAccount(BaseSuperAdminPage):
    def __init__(self, test):
        super(DeleteAccount, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)
        self.LinkedUserList = list.List(self.driver, id="DataTables_Table_0")
        self.LinkedDeviceList = list.List(self.driver, id="DataTables_Table_1")

    # Method to verify page.
    def verify_page(self):
        super(DeleteAccount, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNTS_DELETE)
        return True

    # Method to delete an account.
    def delete_account(self):
        self.Buttons.delete_account.click()
        time.sleep(1)

        # Yes, Delete! button.
        yes_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        yes_delete.click()
        time.sleep(1)

        # Enter DELETE to confirm delete.
        type_delete = self.driver.find_element_by_xpath('/html/body/div[4]/fieldset/input')
        type_delete.clear()
        type_delete.click()
        type_delete.send_keys('DELETE')
        time.sleep(1)

        # Click delete.
        final_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        final_delete.click()
        time.sleep(1)

        # Click final OK button.
        ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        ok.click()
        time.sleep(1)

    # Method to edit account name.
    def edit_account_name(self):
        self.Buttons.edit_account_name.click()
        time.sleep(2)

    # Method to search an user in an account.
    def search_user(self, search_filter):
        elem = self.driver.find_element_by_xpath('(//input[@class="form-control input-sm"])[1]')
        elem.clear()
        elem.click()
        elem.send_keys(search_filter)
        time.sleep(1)

    # Method to select the search user.
    def select_user(self, user_id_link):
        self.search_user(user_id_link)
        user = self.LinkedUserList.get_row_by_index(1)

        if user is not None:
            user.click()
            time.sleep(3)

    # Method to search a device in an account.
    def search_device(self, search_filter):
        elem = self.driver.find_element_by_xpath('(//input[@class="form-control input-sm"])[2]')
        elem.clear()
        elem.click()
        elem.send_keys(search_filter)
        time.sleep(1)

    # Method to select the searched device.
    def select_device(self, device_name):
        self.search_device(device_name)
        device = self.LinkedDeviceList.get_row_by_index(1)

        if device is not None:
            device.click()
            time.sleep(3)

    # Method to select/open profile of an user.
    def linked_user_profile(self):
        profile = self.driver.find_element_by_xpath('//*[@id="user-row-0"]/td[1]')
        profile.click()
        time.sleep(1)


class LinkUser(BaseSuperAdminPage):
    def __init__(self, test):
        super(LinkUser, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(LinkUser, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNTS_LINK_USER)
        return True

    # Method to link an user to an account.
    def link_user(self, user_id_link):
        elem = self.driver.find_element_by_xpath('//*[@id="select-user"]/div[1]/div[1]/span')
        elem.click()
        elem = self.driver.find_element_by_xpath('//*[@id="select-user"]/div[1]/input[1]')
        elem.click()
        elem.send_keys(user_id_link)
        elem = self.driver.find_element_by_xpath('//*[@id="select-user"]/div[1]/ul')
        elem.click()
        elem = self.driver.find_element_by_xpath('//*[@id="select-tu-ex"]')
        elem.click()
        time.sleep(2)
        self.Buttons.link_link_user.click()


class RemoveUser(BaseSuperAdminPage):
    def __init__(self, test):
        super(RemoveUser, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(RemoveUser, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNTS_REMOVE_USER+'/'+user_id_link)
        return True

    # Method to remove/un-link a user from an account.
    def remove_user(self):
        self.Buttons.remove_user.click()
        time.sleep(2)

        # Sure? Delete! button.
        sure_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        sure_delete.click()
        time.sleep(1)


class LinkDevice(BaseSuperAdminPage):
    def __init__(self, test):
        super(LinkDevice, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(LinkDevice, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNTS_LINK_DEVICE)
        return True

    # Method to link a device in an account.
    def link_device(self, device):
        elem = self.driver.find_element_by_xpath('//*[@id="select-device"]/div[1]/div[1]/span/span[2]')
        elem.click()
        elem = self.driver.find_element_by_xpath('//*[@id="select-device"]/div[1]/input[1]')
        elem.clear()
        elem.click()
        elem.send_keys(device)
        elem = self.driver.find_element_by_xpath('//*[@id="select-device"]/div[1]/ul')
        elem.click()
        time.sleep(2)
        self.Buttons.link_link_device.click()


class EditName(BaseSuperAdminPage):
    def __init__(self, test):
        super(EditName, self).__init__(test)
        self.Buttons = buttons. Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(EditName, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNTS_EDIT_NAME + '/' + self.test.account_id)
        return True

    # Method to edit the account name.
    def update_name(self, new_account_name):
        elem = self.driver.find_element_by_xpath('//*[@id="name-input-tenant"]')
        elem.clear()
        elem.click()
        elem.send_keys(new_account_name)
        self.Buttons.update_account_name.click()
        time.sleep(2)
