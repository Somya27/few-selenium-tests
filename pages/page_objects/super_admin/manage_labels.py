from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL
from components import buttons, list, side_menu

import time

MANAGE_LABELS_LIST = BASE_URL + "/" + side_menu.Admin(None).manage_labels['url']
CREATE_LABEL = BASE_URL + "/" + side_menu.Admin(None).create_label['url']


class Labels(BaseSuperAdminPage):
    def __init__(self, test):
        super(Labels, self).__init__(test)
        self.LabelsList = list.List(self.driver, id="DataTables_Table_0")
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(Labels, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_LABELS_LIST)
        return True

    # Method to search a label.
    def search_label(self, search_filter):
        elem = self.driver.find_element_by_xpath('//input[@class="form-control input-sm"]')
        elem.clear()
        elem.click()
        elem.send_keys(search_filter)
        time.sleep(1)

    # Method to select the searched label.
    def select_label(self, label_name):
        self.search_label(label_name)
        label = self.LabelsList.get_row_by_index(1)
        if self.LabelsList.get_cell_from_row(label, 1).text != "No matching records found":
            return label

    # Method to delete a label.
    def delete_label(self, label):
        label_row = self.select_label(label)
        label_button = self.LabelsList.get_cell_from_row(label_row, 3)
        label_button.click()
        time.sleep(2)

        # Yes, Continue! delete button.
        yes_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        yes_delete.click()
        time.sleep(2)

        # Click final OK button to delete.
        done = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        done.click()
        time.sleep(2)


class CreateLabel(BaseSuperAdminPage):
    def __init__(self, test):
        super(CreateLabel, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(CreateLabel, self).verify_page()
        self.test.assertEqual(self.driver.current_url, CREATE_LABEL)
        return True

    # Method to create a new label.
    def create_label(self, tag_select, label_name):
        elem = self.driver.find_element_by_xpath('//*[@id="select-tag"]/div[1]/div[1]/span')
        elem.click()
        elem = self.driver.find_element_by_xpath('//*[@id="select-tag"]/div[1]/input[1]')
        elem.click()
        elem.send_keys(tag_select)
        time.sleep(1)
        elem = self.driver.find_element_by_xpath('//*[@id="select-tag"]/div[1]/ul')
        elem.click()
        elem = self.driver.find_element_by_xpath('//*[@id="input-label"]')
        elem.send_keys(label_name)
        time.sleep(2)
        self.Buttons.save_label.click()

