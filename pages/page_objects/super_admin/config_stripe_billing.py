from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL
from components import buttons, side_menu

import time

CONFIG_STRIPE_BILLING = BASE_URL + "/" + side_menu.Admin(None).config_stripe_billing['url']


class ConfigStripe(BaseSuperAdminPage):
    def __init__(self, test):
        super(ConfigStripe, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(ConfigStripe, self).verify_page()
        self.test.assertEqual(self.driver.current_url, CONFIG_STRIPE_BILLING)
        return True

    # Method for Stripe configuration on the portal.
    def stripe_config(self, secret_key, publishable_key):
        elem = self.driver.find_element_by_xpath('//*[@id="secret-key"]')
        elem.clear()
        elem.click()
        elem.send_keys(secret_key)
        elem = self.driver.find_element_by_xpath('//*[@id="publishable-key"]')
        elem.clear()
        elem.click()
        elem.send_keys(publishable_key)

    # Method to delete existing Stripe configuration from the portal.
    def delete_stripe_config(self):
        self.Buttons.delete_stripe_configuration.click()
        time.sleep(2)

        # Yes, Delete! button.
        yes_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        yes_delete.click()
        time.sleep(1)
