from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL
from components import buttons, list, side_menu

import time

MANAGE_ACCOUNT_USERS_LIST = BASE_URL + "/" + side_menu.Admin(None).manage_account_users['url']
MANAGE_ACCOUNT_USERS_INVITE = BASE_URL + "/" + side_menu.Admin(None).invite_users['url']
MANAGE_ACCOUNT_USER_DELETE = BASE_URL + "/" + side_menu.Admin(None).delete_user['url']


class UserList(BaseSuperAdminPage):
    def __init__(self, test):
        super(UserList, self).__init__(test)
        self.UsersList = list.List(self.driver, id="DataTables_Table_0")
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(UserList, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNT_USERS_LIST)
        return True

    # Method to search an user.
    def search_user(self, search_filter):
        elem = self.driver.find_element_by_xpath('//input[@class="form-control input-sm"]')
        elem.clear()
        elem.click()
        elem.send_keys(search_filter)
        time.sleep(1)

    # Method to select the searched user.
    def select_user(self, user_name):
        self.search_user(user_name)
        user = self.UsersList.get_row_by_index(1)

        if user is not None:
            user.click()
            time.sleep(0.5)

    # Method to click final ok button to remove/unlink an user from an account.
    # This method is called inside manage_accounts_test.py.
    def remove_user_ok(self):
        # Click final OK button.
        ok = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        ok.click()
        time.sleep(1)


class InviteUser(BaseSuperAdminPage):
    def __init__(self, test):
        super(InviteUser, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(InviteUser, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNT_USERS_INVITE)
        return True

    # Method to invite a new user to the portal.
    def invite_new_user(self, user_id, user_name):
        elem = self.driver.find_element_by_xpath('//*[@id="login-input-user"]')
        elem.clear()
        elem.click()
        elem.send_keys(user_id)
        elem = self.driver.find_element_by_xpath('//*[@id="name-input-user"]')
        elem.clear()
        elem.click()
        elem.send_keys(user_name)
        time.sleep(1)
        self.Buttons.invite_invite.click()

    # Method to click cancel while inviting a new user.
    def cancel_new_user(self, user_name):
        elem = self.driver.find_element_by_xpath('//*[@id="login-input-user"]')
        elem.send_keys(user_name)
        time.sleep(1)
        self.Buttons.cancel_invite.click()


class DeleteUser(BaseSuperAdminPage):
    def __init__(self, test):
        super(DeleteUser, self).__init__(test)
        self.Buttons = buttons.Buttons(self.driver)

    # Method to verify page.
    def verify_page(self):
        super(DeleteUser, self).verify_page()
        self.test.assertEqual(self.driver.current_url, MANAGE_ACCOUNT_USER_DELETE+'/'+self.test.user_id)
        return True

    # Method to delete an user from the portal.
    def delete_user(self):
        self.Buttons.delete_user.click()
        time.sleep(1)

        # Yes, Delete! button.
        yes_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        yes_delete.click()
        time.sleep(1)

