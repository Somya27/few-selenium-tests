from pages.base_page import BaseSuperAdminPage
from test.configuration import BASE_URL
from components import buttons, list, side_menu

import time

CONFIG_DASHBOARDS_LIST = BASE_URL + "/" + side_menu.Admin(None).config_dashboards['url']
CONFIG_DASHBOARDS_ADD_PAGE = BASE_URL + "/" + side_menu.Admin(None).config_dashboards_add['url']


class ConfigDashboardList(BaseSuperAdminPage):
    def __init__(self, test):
        super(ConfigDashboardList, self).__init__(test)
        # Define what components we have on the page
        self.PageList = list.List(self.driver, id="table")
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(ConfigDashboardList, self).verify_page()
        self.test.assertEqual(self.driver.current_url, CONFIG_DASHBOARDS_LIST)
        return True

    def search_page(self, page_name):
        elem = self.driver.find_element_by_xpath('//*[@id="table_filter"]/label/input')
        elem.clear()
        elem.click()
        elem.send_keys(page_name)
        time.sleep(1)

    # Method to select the searched page.
    def select_page(self, page_name):
        self.search_page(page_name)
        page_row = self.PageList.get_row_by_index(1)
        if self.PageList.get_cell_from_row(page_row, 2).text == page_name:
            return page_row

    # Method to delete a page.
    def delete_page(self, page_name):
        page_row = self.select_page(page_name)
        page_button = self.PageList.get_cell_from_row(page_row, 4)
        page_button.click()
        time.sleep(2)

        # Yes, Continue! delete button.
        yes_delete = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        yes_delete.click()
        time.sleep(2)

        # Click final OK button to delete.
        done = self.driver.find_element_by_xpath('/html/body/div[4]/div[7]/div/button')
        done.click()
        time.sleep(2)



class AddConfigDashboard(BaseSuperAdminPage):
    def __init__(self, test):
        super(AddConfigDashboard, self).__init__(test)
        # Define what components we have on the page
        self.Buttons = buttons.Buttons(self.driver)

    def verify_page(self):
        super(AddConfigDashboard, self).verify_page()
        self.test.assertEqual(self.driver.current_url, CONFIG_DASHBOARDS_ADD_PAGE + "/new")
        return True

    def show_device_data(self, page_name):
        elem = self.driver.find_element_by_xpath('//*[@id="page_name"]')
        elem.send_keys(page_name)
        time.sleep(1)
        self.Buttons.save_and_activate_dashboard.click()

    def load_last_saved(self):
        elem = self.driver.find_element_by_xpath('//*[@id="codemirror"]/div/div[6]')
        elem.send_keys()
        self.Buttons.load_last_saved_dashboard.click()

    def load_original(self):
        elem = self.driver.find_element_by_xpath('//*[@id="codemirror"]/div/div[6]')
        elem.send_keys()
        self.Buttons.load_original_dashboard.click()


