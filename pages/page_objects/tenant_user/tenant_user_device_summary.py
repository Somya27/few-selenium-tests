from pages.base_page import BaseTenantUserPage
from test.configuration import BASE_URL
from components import side_menu


TU_DEVICE_SUMMARY = BASE_URL + "/" + side_menu.TenantAsUser(None).tu_device_summary['url']
TU_DEVICE_DETAIL = BASE_URL + "/" + side_menu.TenantAsUser(None).tu_device_detail['url']


class DeviceSummaryTenantUser(BaseTenantUserPage):
    def __init__(self, test):
        super(DeviceSummaryTenantUser, self).__init__(test)

    def verify_page(self):
        super(DeviceSummaryTenantUser, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TU_DEVICE_SUMMARY)
        return True

    def device_summary_tu(self):
        elem = self.driver.find_element_by_xpath('//*[@id="page-wrapper"]/div/div/div[2]/div/div/div/div[1]')
        elem.click()


class DeviceDetailTenantUser(BaseTenantUserPage):
    def __init__(self, test):
        super(DeviceDetailTenantUser, self).__init__(test)

    def verify_page(self):
        super(DeviceDetailTenantUser, self).verify_page()
        self.test.assertEqual(self.driver.current_url, TU_DEVICE_DETAIL + "/" + self.test.device_id)
        return True





