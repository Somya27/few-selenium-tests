from selenium.webdriver.common.keys import Keys
from test.configuration import BASE_URL

LOGIN_URL = BASE_URL + "/login"


class Login(object):

    def __init__(self, test):
        self.test = test
        self.driver = test.driver
        self.driver.get(LOGIN_URL)

    def verify_page(self):
        self.test.assertEqual(self.driver.current_url, LOGIN_URL)
        #TODO: verify page
        return True

    def login(self, username, password):
        #TODO: we don't test clicking the login button, hitting enter instead
        elem = self.driver.find_element_by_name("email")
        elem.send_keys(username)
        elem = self.driver.find_element_by_name("password")
        elem.send_keys(password, Keys.RETURN)

    def goto_signup_page(self):
        elem = self.driver.find_element_by_id("signup-button")
        elem.click()


