from test.configuration import SHORT_WAIT, LONG_WAIT
from components.side_menu import Admin, TenantAsAdmin, TenantAsUser
from components.top_menu import TopMenu
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, test):
        # bind the test framework and driver
        self.test = test
        self.driver = test.driver
        # Define what components we have on the base page
        self.TopMenu = TopMenu(self.driver)
        # self.SideMenu = Admin(self.driver)

    def page_should_be(self, cls):
        page = cls(self)
        self.driver.assertTrue(page.verify_page())
        return page

    def verify_page(self):
        # wait briefly for loading bar if the page needs to load
        wait = WebDriverWait(self.driver, SHORT_WAIT)
        try:
            wait.until(EC.visibility_of_element_located((By.ID, "loading-bar")))
        except TimeoutException:
            # if we time out here, the page is already loaded
            pass
        # wait for loading to complete (long wait)
        wait = WebDriverWait(self.driver, LONG_WAIT)
        wait.until(EC.invisibility_of_element_located((By.ID, "loading-bar")))


class BaseSuperAdminPage(BasePage):

    def __init__(self, test):
        super(BaseSuperAdminPage, self).__init__(test)
        # override SideMenu for Admin page
        self.SideMenu = Admin(self.driver)
        # self.TopMenu = TopMenu(self.driver)


class BaseTenantAdminPage(BasePage):

    def __init__(self, test):
        super(BaseTenantAdminPage, self).__init__(test)
        # override SideMenu for Admin page
        self.SideMenu = TenantAsAdmin(self.driver)
        # self.TopMenu = TopMenu(self.driver)


class BaseTenantUserPage(BasePage):

    def __init__(self, test):
        super(BaseTenantUserPage, self).__init__(test)
        # override SideMenu for Admin page
        self.SideMenu = TenantAsUser(self.driver)
        # self.TopMenu = TopMenu(self.driver)
